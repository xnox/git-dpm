Format: 3.0 (quilt)
Source: bbbb
Binary: bbbb
Architecture: all
Version: 0-2
Maintainer: Me <me@here.tldg>
Checksums-Sha1: 
 d4604050e3e62d5922b2d111b8d695edb5e606d2 138 bbbb_0.orig-baz.tar.bz2
 d10c6a11bcf4245d36459f63435613fa656a0641 164 bbbb_0.orig-bla.tar.gz
 374b845c30928c4c12aaf3b95ac5b24db6dcebb3 168 bbbb_0.orig.tar.bz2
 132fc5e402f9068f59abc5cbe8c11f1aeb715ae7 567 bbbb_0-2.debian.tar.gz
Checksums-Sha256: 
 513e0a902b6058a9b5bc4915f085d7fbadd67921e2134f6049c9e936f6c4ee1e 138 bbbb_0.orig-baz.tar.bz2
 22dcf6cc8b65d56412e0cc30a18bd5c7d72b5d9bc1c35c4862bdb71dc12ddaa8 164 bbbb_0.orig-bla.tar.gz
 bb9bdd089015b5ab70d43280a1c5f635193dd71139b0662eca3a8faf722efcf9 168 bbbb_0.orig.tar.bz2
 932b009c0f4e3ad17f3361db3aa3ab2e319ff513ce992224c8b3f4ce6299dd7c 567 bbbb_0-2.debian.tar.gz
Files: 
 97256575dc064e3a0e5bd28270ea0437 138 bbbb_0.orig-baz.tar.bz2
 c51a59cd39190bd10eefe911dc1886bd 164 bbbb_0.orig-bla.tar.gz
 ceb246c9b58353529376486a4058264f 168 bbbb_0.orig.tar.bz2
 3c53aeedb33c8a19b8d1ba081a57df43 567 bbbb_0-2.debian.tar.gz
