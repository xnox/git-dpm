Format: 1.0
Source: aaaa
Binary: done
Architecture: all
Version: 0-2
Maintainer: Nobody <nobody@nowhere>
Standards-Version: 3.9.0
Build-Depends: debhelper (>= 5), quilt
Checksums-Sha1: 
 0b76a8741ca3334c168ca55187a7ff5e2fd72f64 352 aaaa_0.orig.tar.gz
 b0f85f1ff01f2d0a3e10e0dec21b56926a58872e 1069 aaaa_0-2.diff.gz
Checksums-Sha256: 
 aa6ecb5f14669008b5b9be27a0fa764698f4215d7a928be67ecd3169d65f650a 352 aaaa_0.orig.tar.gz
 dc9b18fc55d9b654d32db439850ecc350c51d1c931b6311d1519d84f45d4d52a 1069 aaaa_0-2.diff.gz
Files: 
 e924b00324be1f8194c7115b7fc508fd 352 aaaa_0.orig.tar.gz
 4e5419a2b0e2d306ca832742136abea5 1069 aaaa_0-2.diff.gz
