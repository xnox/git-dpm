#!/bin/sh

set -e

export LC_ALL=C

gitdpmaddoptions=""
SRCDIR=""
testtodo="all"
inspect=false
dpmprefix=""
while test $# -gt 0 ; do
	case "$1" in
		-x)
			dpmprefix="bash -x "
			;;
		--srcdir)
			shift
			SRCDIR="$1"
			;;
		--debug)
			gitdpmaddoptions="$gitdpmaddoptions --debug"
			;;
		--debug-git-calls)
			gitdpmaddoptions="$gitdpmaddoptions --debug-git-calls"
			;;
		--inspect)
			inspect=true
			;;
		--test)
			shift
			testtodo="$1"
			;;
		*)
			echo "Unknown argument $1" >&2
			exit 1
			;;
	esac
	shift
done

if test -z "$SRCDIR" ; then
	SRCDIR="$(readlink -e "$(dirname $0)"/..)"
fi
gitdpm="$SRCDIR"/git-dpm.sh
if ! test -f "$gitdpm" ; then
	echo "Cannot find $gitdpm" >&2
	exit 3
fi
gitdpm="$dpmprefix$gitdpm $gitdpmaddoptions"
EXDIR="$SRCDIR"/tests/example
if ! test -d "$EXDIR" ; then
	echo "Cannot find $EXDIR" >&2
	exit 3
fi

if test -z "$TMPDIR" ; then
	TMPDIR="$TEMPDIR"
fi
if test -z "$TMPDIR" ; then
	TMPDIR="/tmp"
fi

# note the evil space character!
TESTDIR="$(mktemp -p "$TMPDIR" -d "git-dpm test.XXXXXXXXX")"
if ! test -d "$TESTDIR" || ! test -O "$TESTDIR" ; then
	echo "Error creating temporary testing directory!" >&2
	exit 3
fi

cd "$TESTDIR" || exit 3
mkdir test || exit 3
cd test || exit 3
git init
git config --add user.name "John Comitter"
git config --add user.email "local@nowhere"
# This is the default, but override something possibly found in ~/
git config --add dpm.importWithoutParent true

exitmessage="An error occured."
exitcode=1

onexit() {
	echo "$exitmessage"
	if $inspect ; then
		echo "You are not in a shell in the temporary directory the tests run it."
		echo "When you exit that directory will be removed."
		$SHELL
	fi
	rm -rf "$TESTDIR/test/.git"
	rm -r "$TESTDIR"
	exit $exitcode
}
trap onexit exit

deterministic_log() {
	if test x"$1" = x"noauthor" ; then
	(git for-each-ref --format 'tag|%(refname)|%(objectname)' "refs/tags/*" | sort ; git log --topo-order --reverse --pretty=tformat:"%H|%s|%P" ) | awk 'BEGIN{FS="|"} $1 == "tag" {t = tagnames[$3]; refname=gensub("refs/tags/", "", 1, $2);  if ( t == "" ) { tagnames[$3] = ", tags: " refname ; } else { tagnames[$3] = t ", " refname} ; next} {name[$1] = $2 ; pn = split($3, p, " "); r = $2 ; if ( pn > 0 ) { r = r ", parents: " name[p[1]]; } for ( i = 2 ; i <= pn ; i++ ) { r = r ", " name[p[i]];}; print r tagnames[$1] }' | sort
	else
	(git for-each-ref --format 'tag|%(refname)|%(objectname)' "refs/tags/*" | sort ; git log --topo-order --reverse --pretty=tformat:"%H|%s|%P|%an <%ae>" ) | awk 'BEGIN{FS="|"} $1 == "tag" {t = tagnames[$3]; refname=gensub("refs/tags/", "", 1, $2);  if ( t == "" ) { tagnames[$3] = ", tags: " refname ; } else { tagnames[$3] = t ", " refname} ; next} {name[$1] = $2 ; pn = split($3, p, " "); r = $2 ; if ( pn > 0 ) { r = r ", parents: " name[p[1]]; } for ( i = 2 ; i <= pn ; i++ ) { r = r ", " name[p[i]];}; print r ", author: " $4 tagnames[$1] }' | sort
	fi
}

compare_results() {
	echo "Checking results for $1"
	if ! diff -u ../results.expected ../results ; then
		echo "Incorrect results from $1" >&2
		exit 1
	fi
}

check_gitlog() {
	deterministic_log "$2" > ../results
	cat > ../results.expected
	compare_results "$1"
}

sometestsfound=false
testcaseenabled() {
	if test x"$testtodo" != x"$1" && test x"$testtodo" != x"all" ; then
		return 1
	else
		sometestsfound=true
		echo "################################# $1 ###############################"
		return 0
	fi
}
if testcaseenabled mergepatched ; then
set -e

$gitdpm empty-tree "test"
# in the new upstream each file can either be:
# unchanged, changed, removed, added
# the debian branch can do the same
echo a > a
echo b > b
echo normal-deleted > normal-deleted
mkdir deep debian
echo subdir-deleted > deep/subdir-deleted
echo da > debian/a
git add a b debian/a normal-deleted deep/subdir-deleted
for d in unchanged changed removed ; do
	for u in unchanged changed removed ; do
		mkdir deep/$u-$d $u-$d
		echo "$u-$d-ntest" > $u-$d/.gitignore
		echo "$u-$d-dtest" > deep/$u-$d/.gitignore
		echo "$u-$d-ttest" > .gittest_$u-$d
		git add $u-$d/.gitignore deep/$u-$d/.gitignore .gittest_$u-$d
	done
done
git commit --amend -m "many.gitfiles"
git tag tag.oldupstream
for d in unchanged changed removed ; do
	u=removed
	git rm $u-$d/.gitignore deep/$u-$d/.gitignore .gittest_$u-$d
	u=added
	if test $u = unchanged ; then
		mkdir deep/$u-$d $u-$d
		echo "$u-$d-nTest" > $u-$d/.gitignore
		echo "$u-$d-dTest" > deep/$u-$d/.gitignore
		echo "$u-$d-tTest" > .gittest_$u-$d
		git add $u-$d/.gitignore deep/$u-$d/.gitignore .gittest_$u-$d
	fi
	u=changed
	echo "$u-$d-nTest" > $u-$d/.gitignore
	echo "$u-$d-dTest" > deep/$u-$d/.gitignore
	echo "$u-$d-tTest" > .gittest_$u-$d
	git add $u-$d/.gitignore deep/$u-$d/.gitignore .gittest_$u-$d
done
	d=added
	u=added
	mkdir deep/$u-$d $u-$d
	echo "$u-$d-nTest" > $u-$d/.gitignore
	echo "$u-$d-dTest" > deep/$u-$d/.gitignore
	echo "$u-$d-tTest" > .gittest_$u-$d
	git add $u-$d/.gitignore deep/$u-$d/.gitignore .gittest_$u-$d
git commit -m "mods.gitfiles"
git tag tag.newupstream

git checkout -b master tag.oldupstream
echo "oldpatched" > oldpatched
git add oldpatched
git commit -m "oldpatched"
git tag tag.oldpatched

for u in unchanged changed removed ; do
	d=removed
	git rm $u-$d/.gitignore deep/$u-$d/.gitignore .gittest_$u-$d
	d=changed
	echo "$u-$d-nD" > $u-$d/.gitignore
	echo "$u-$d-dD" > deep/$u-$d/.gitignore
	echo "$u-$d-tD" > .gittest_$u-$d
	git add $u-$d/.gitignore deep/$u-$d/.gitignore .gittest_$u-$d
done
for u in added ; do
	d=added
	mkdir deep/$u-$d $u-$d
	echo "$u-$d-nD" > $u-$d/.gitignore
	echo "$u-$d-dD" > deep/$u-$d/.gitignore
	echo "$u-$d-tD" > .gittest_$u-$d
	git add $u-$d/.gitignore deep/$u-$d/.gitignore .gittest_$u-$d
done
echo control > debian/control
git add debian/control
git rm debian/a
git rm normal-deleted deep/subdir-deleted
git commit -m "master"
touch ../test.orig.tar.gz
touch ../test2.orig.tar.gz
filterlstree() {
	sed -e 's/^100644 blob \([0-9a-f]*\)	\([^ 	]*\)$/\2	\1/'
}
filteredlstree() {
	git ls-tree -r "tag.$1" | filterlstree > ../tree."$1"

}

git ls-tree -r HEAD | filterlstree > ../tree.olddebianbeforeinit
$gitdpm init ../test.orig.tar.gz tag.oldupstream tag.oldpatched tag.oldpatched
rm ../test.orig.tar.gz
git tag tag.olddebian
filteredlstree olddebian

sed -e '/^debian\/\.git-dpm	/d' -e '/^debian\/patches/d' -e '/^debian\/source\/format	/d' ../tree.olddebian > ../tree.olddebianwithoutnew

if test $(wc -l < ../tree.olddebian) != $(( 4 + $(wc -l < ../tree.olddebianbeforeinit) )) ||
       ! diff -u ../tree.olddebianbeforeinit ../tree.olddebianwithoutnew > /dev/null; then
	echo "Test mergepatched: Wrong results from init:" >&2
	diff -u ../tree.olddebianbeforeinit ../tree.olddebian >&2
	exit 1
fi

git update-ref refs/heads/upstream tag.newupstream tag.oldupstream
$gitdpm new-upstream ../test2.orig.tar.gz
rm ../test2.orig.tar.gz
git tag tag.newupstreamrecorded
git checkout -b patched tag.newupstream
echo "patched" > patched
git add patched
git commit -m "patched"
git tag tag.patched

filteredlstree oldupstream
filteredlstree newupstream
filteredlstree patched

$gitdpm merge-patched
git ls-tree -r HEAD | filterlstree | sed -e '/^debian\/\.git-dpm	/d' | sort > ../results

sed -n -e '/^debian\/\.git-dpm	/d' -e '/^debian\//p' ../tree.olddebian > ../tree.u1
sed -e '/^debian\//d' -e '/-deleted/d' -e '/-added/d' -e '/-changed/d' -e '/-removed/d'  ../tree.patched >> ../tree.u1
sed -n -e '/-added/p' -e '/-changed/p' ../tree.olddebian >> ../tree.u1
sort ../tree.u1 > ../results.expected

compare_results "mergepatched with default"

git update-ref refs/heads/patched tag.patched ""
git reset --hard tag.newupstreamrecorded

$gitdpm merge-patched --dot-git-files=upstream
git ls-tree -r HEAD | filterlstree | sed -e '/^debian\/\.git-dpm	/d' | sort > ../results

sed -n -e '/^debian\/\.git-dpm	/d' -e '/^debian\//p' ../tree.olddebian > ../tree.u
sed -e '/^debian\//d' -e '/-deleted/d' -e '/-removed/d' ../tree.patched >> ../tree.u
sort ../tree.u > ../results.expected

compare_results "mergepatched with --dot-git-files=upstream"

git update-ref refs/heads/patched tag.patched ""
git reset --hard tag.newupstreamrecorded

$gitdpm merge-patched --dot-git-files=upstream --no-deletions
git ls-tree -r HEAD | filterlstree | sed -e '/^debian\/\.git-dpm	/d' | sort > ../results

sed -n -e '/^debian\/\.git-dpm	/d' -e '/^debian\//p' ../tree.olddebian > ../tree.u
sed -e '/^debian\//d' ../tree.patched >> ../tree.u
sort ../tree.u > ../results.expected

compare_results "mergepatched with --dot-git-files=upstream --no-deletions"

git update-ref refs/heads/patched tag.patched ""
git reset --hard tag.newupstreamrecorded

$gitdpm merge-patched --no-deletions
git ls-tree -r HEAD | filterlstree | sed -e '/^debian\/\.git-dpm	/d' | sort > ../results

sed -n -e '/^debian\/\.git-dpm	/d' -e '/^debian\//p' ../tree.olddebian > ../tree.u
sed -e '/^debian\//d' -e '/-added/d' -e '/-changed/d' -e '/-removed/d'  ../tree.patched >> ../tree.u
sed -n -e '/-added/p' -e '/-changed/p' ../tree.olddebian >> ../tree.u
sort ../tree.u > ../results.expected

compare_results "mergepatched with --no-deletions"

$gitdpm empty-tree "test"
git tag -d tag.oldupstream tag.newupstream tag.newupstreamrecorded tag.patched tag.olddebian tag.oldpatched
git branch -D upstream master
rm ../tree.*
git tag
git branch
fi

if testcaseenabled importtar ; then

EDITOR=true $gitdpm import-tar -- "$EXDIR"/nobasedir.tar.gz
git ls-tree HEAD >../results
cat > ../results.expected <<EOF
100644 blob 78981922613b2afb6025042ff6bd878ac1994e85	a
100644 blob 61780798228d17af2d34fce4cfbdf35556832472	b
EOF
compare_results "import-tar nobasedir"
EDITOR=true $gitdpm import-tar -- "$EXDIR"/evil.tar.gz
git ls-tree HEAD >../results
cat > ../results.expected <<EOF
100644 blob 1263948fb882b8c4fd639b01e17969c825e79619	Makefile
EOF
compare_results "import-tar evil"
if test -e .git/hooks/evil ; then
	echo "Evil tar does evil!" >&2
	exit 1
fi

fi
if testcaseenabled importdsc ; then

$gitdpm import-dsc --branch master "$EXDIR"/aaaa_0-1.dsc
$gitdpm import-dsc --branch master "$EXDIR"/aaaa_0-2.dsc
$gitdpm import-dsc --branch master "$EXDIR"/aaaa_0-3.dsc
git log --graph | cat

check_gitlog "import-dsc" <<'EOF'
Import aaaa 0-1, parents: Import aaaa_0-1.dsc, author: John Comitter <local@nowhere>
Import aaaa 0-2, parents: Import aaaa 0-1, Import aaaa_0-2.dsc, really-done, author: John Comitter <local@nowhere>
Import aaaa 0-3, parents: Import aaaa 0-2, Import aaaa_0-3.dsc, echo "really done" instead of "done"., author: John Comitter <local@nowhere>
Import aaaa_0-1.dsc, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
Import aaaa_0-2.dsc, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
Import aaaa_0-3.dsc, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
echo "really done" instead of "done"., parents: Import aaaa_0.orig.tar.gz, author: Joe Patcher <patcher@nowhere>
really-done, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
EOF

$gitdpm import-dsc --branch au "$EXDIR"/aaaa_0-1.dsc
$gitdpm import-dsc --author "Original Maintainer <maint@nowhere>" --branch au "$EXDIR"/aaaa_0-2.dsc
$gitdpm import-dsc --author "Original Maintainer <maint@nowhere>" --branch au "$EXDIR"/aaaa_0-3.dsc

check_gitlog "import-dsc" <<'EOF'
Import aaaa 0-1, parents: Import aaaa_0-1.dsc, author: John Comitter <local@nowhere>
Import aaaa 0-2, parents: Import aaaa 0-1, Import aaaa_0-2.dsc, really-done, author: John Comitter <local@nowhere>
Import aaaa 0-3, parents: Import aaaa 0-2, Import aaaa_0-3.dsc, echo "really done" instead of "done"., author: John Comitter <local@nowhere>
Import aaaa_0-1.dsc, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
Import aaaa_0-2.dsc, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
Import aaaa_0-3.dsc, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
echo "really done" instead of "done"., parents: Import aaaa_0.orig.tar.gz, author: Original Maintainer <maint@nowhere>
really-done, parents: Import aaaa_0.orig.tar.gz, author: Original Maintainer <maint@nowhere>
EOF

$gitdpm import-dsc --branch defau "$EXDIR"/aaaa_0-1.dsc
git rm .stamp
EDITOR=true git commit --amend
EDITOR=true $gitdpm update-patches --amend

find debian -maxdepth 1 -name patches -empty -delete
git clean -n -d > ../results
true > ../results.expected
compare_results "workdir clean after update-patches(1)"

$gitdpm tag --named
$gitdpm import-dsc --default-author="Original Maintainer <maint@nowhere>" --branch defau "$EXDIR"/aaaa_0-2.dsc
git rm .stamp
EDITOR=true git commit --amend
EDITOR=true $gitdpm update-patches --amend
find debian -maxdepth 1 -name patches -empty -delete
git clean -n -d > ../results
true > ../results.expected
compare_results "workdir clean after update-patches(2)"
$gitdpm tag --named
$gitdpm import-dsc --default-author "Original Maintainer <maint@nowhere>" --branch defau "$EXDIR"/aaaa_0-3.dsc
git rm .stamp
EDITOR=true git commit --amend
EDITOR=true $gitdpm update-patches --amend
find debian -maxdepth 1 -name patches -empty -delete
git clean -n -d > ../results
true > ../results.expected
compare_results "workdir clean after update-patches(3)"
$gitdpm tag --named

check_gitlog "import-dsc with --default-author and update-patches called" <<'EOF'
Import aaaa 0-1, parents: Import aaaa_0-1.dsc, author: John Comitter <local@nowhere>, tags: aaaa-debian-0-1
Import aaaa 0-2, parents: Import aaaa 0-1, Import aaaa_0-2.dsc, really-done, author: John Comitter <local@nowhere>, tags: aaaa-debian-0-2
Import aaaa 0-3, parents: Import aaaa 0-2, Import aaaa_0-3.dsc, echo "really done" instead of "done"., author: John Comitter <local@nowhere>, tags: aaaa-debian-0-3
Import aaaa_0-1.dsc, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
Import aaaa_0-2.dsc, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
Import aaaa_0-3.dsc, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>, tags: aaaa-patched-0-1, aaaa-upstream-0
echo "really done" instead of "done"., parents: Import aaaa_0.orig.tar.gz, author: Joe Patcher <patcher@nowhere>, tags: aaaa-patched-0-3
really-done, parents: Import aaaa_0.orig.tar.gz, author: Original Maintainer <maint@nowhere>, tags: aaaa-patched-0-2
EOF

EDITOR=true $gitdpm --debug --debug-git-calls import-new-upstream --rebase-patched "$EXDIR/aaaa_1.orig.tar.gz"
pristine-tar commit "$EXDIR/aaaa_1.orig.tar.gz" upstream-defau
git checkout defau
dch -v 1-1 "New upstream version (including all patches)"
EDITOR=true git commit --amend -a
EDITOR=true $gitdpm update-patches --amend
sed -i debian/rules -e '/^include/d' -e 's/ unpatch$//' -e 's/ $(QUILT_STAMPFN)$//'
sed -i debian/control -e 's/, quilt$//'
dch "Change to new source format"
# keep commit message short to make things easier later in this script
git commit --amend -a -m 'Prepare aaaa 1-1'
$gitdpm tag --named
git log --decorate --graph | cat
check_gitlog "import-new-upstream and update-patches" <<'EOF'
Import aaaa 0-1, parents: Import aaaa_0-1.dsc, author: John Comitter <local@nowhere>, tags: aaaa-debian-0-1
Import aaaa 0-2, parents: Import aaaa 0-1, Import aaaa_0-2.dsc, really-done, author: John Comitter <local@nowhere>, tags: aaaa-debian-0-2
Import aaaa 0-3, parents: Import aaaa 0-2, Import aaaa_0-3.dsc, echo "really done" instead of "done"., author: John Comitter <local@nowhere>, tags: aaaa-debian-0-3
Import aaaa_0-1.dsc, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
Import aaaa_0-2.dsc, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
Import aaaa_0-3.dsc, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>
Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>, tags: aaaa-patched-0-1, aaaa-upstream-0
Import aaaa_1.orig.tar.gz, parents: Import aaaa_0.orig.tar.gz, author: John Comitter <local@nowhere>, tags: aaaa-patched-1-1, aaaa-upstream-1
Prepare aaaa 1-1, parents: Import aaaa 0-3, Import aaaa_1.orig.tar.gz, author: John Comitter <local@nowhere>, tags: aaaa-debian-1-1
echo "really done" instead of "done"., parents: Import aaaa_0.orig.tar.gz, author: Joe Patcher <patcher@nowhere>, tags: aaaa-patched-0-3
really-done, parents: Import aaaa_0.orig.tar.gz, author: Original Maintainer <maint@nowhere>, tags: aaaa-patched-0-2
EOF
cp ../results.expected ../results.saved

# TODO: check if merge-patched correctly removed .stamp from debian branch

cat > .gitignore <<EOF
/.stamp
/done
/debian/done
/debian/files
/debian/*.log
/debian/done.substvars
/build-indep-stamp
EOF
git add .gitignore
git commit -m "z add .gitignore"

# dpkg-buildpackage would fail here because of space issued...
# so fix that...

$gitdpm checkout-patched
sed -i Makefile -e 's#\${DESTDIR}/usr/bin/done$#"&"#'
git commit -m "z fix problems when DESTDIR contains spaces" -a
# Let's do some non-"--amend" commits otherwise it gets boring...
EDITOR=true $gitdpm update-patches

if ! test -d debian/patches ; then
	echo "No debian/patches after update-patches!" >&2
	exit 1
fi
if ! test -f debian/patches/series ; then
	echo "No debian/patches/series after update-patches!" >&2
	exit 1
fi
wc -l debian/patches/series > ../results
cat > ../results.expected <<'EOF'
1 debian/patches/series
EOF
compare_results "debian/patches after update-patches"

git clean -n -d > ../results
true > ../results.expected
compare_results "workdir clean after update-patches"

sed -i debian/rules -e 's/DESTDIR=\(.*\)/DESTDIR="\1"/'
dch "fix problems when building in a directory containing spaces"
git commit --amend -a -m "z fix problems when building in a directory containing spaces"


# someone must have been a bit hasty calling tag before it compiles,
# luckily there is --refresh...
$gitdpm tag --named --refresh

# the z in the commit messages allow appending here...
sed ../results.saved -e 's/aaaa-patched-1-1, //' -e 's/, tags: aaaa-debian-1-1//' > ../results.expected
cat >>../results.expected <<'EOF'
z add .gitignore, parents: Prepare aaaa 1-1, author: John Comitter <local@nowhere>
z fix problems when DESTDIR contains spaces, parents: Import aaaa_1.orig.tar.gz, author: John Comitter <local@nowhere>, tags: aaaa-patched-1-1
z fix problems when building in a directory containing spaces, parents: z add .gitignore, z fix problems when DESTDIR contains spaces, author: John Comitter <local@nowhere>, tags: aaaa-debian-1-1
EOF
deterministic_log > ../results
compare_results "new patch test"

rm ../results ../results.expected
$gitdpm prepare
if ! test -f "../aaaa_1.orig.tar.gz" ; then
	echo "prepare did not create aaaa_1.orig.tar.gz as it should have" >&2
	exit 1
fi

dpkg-buildpackage -rfakeroot -us -uc
git clean -n -d > ../results
true > ../results.expected
compare_results "everything ignored test"

fakeroot debian/rules clean
git clean -n -x -d > ../results
true > ../results.expected
compare_results "everything ignored test"

fi
if testcaseenabled init ; then

EDITOR=true $gitdpm import-tar "$EXDIR"/some.orig.tar.gz
git ls-tree -r HEAD >../results
cat > ../results.expected <<EOF
100644 blob 822216280fd1de308f55f27cdd7a4196a8f5a189	debian/cruft
100644 blob 06baef206a8179d004448aca0d8e61b312bc397a	lines
100644 blob f00c965d8307308469e537302baa73048488f162	numbers
EOF
compare_results "import-tar some.orig"
git branch some-upstream

seq 30 10 200 > morenumbers
sed -e '/^4/d' -i numbers
git add numbers morenumbers
git commit -m "some patch"

git tag patches

git checkout -b some some-upstream
git tag upstream1

git rm -r debian
mkdir debian
echo "bla" > debian/control
git add debian/control
git commit -m "add debian"

$gitdpm init "$EXDIR/some.orig.tar.gz" some-upstream some-upstream patches

git ls-tree -r HEAD | sed -e 's# [0-9a-f]*\(	debian/\(.git-dpm\|patches\)\)# X\1#' >../results
cat > ../results.expected <<EOF
100644 blob X	debian/.git-dpm
100644 blob a7f8d9e5dcf3a68fdd2bfb727cde12029875260b	debian/control
100644 blob X	debian/patches/0001-some-patch.patch
100644 blob X	debian/patches/series
100644 blob 163aaf8d82b6c54f23c45f32895dbdfdcc27b047	debian/source/format
100644 blob 06baef206a8179d004448aca0d8e61b312bc397a	lines
100644 blob 56d8e57bf9f9b45c15d8aacef6f00d1f48c75139	morenumbers
100644 blob 051c29820ed106f8865c540dc6483142b5e490c8	numbers
EOF
compare_results "tree of init (with debian branch)"
if ! grep '^From: John Comitter <local@nowhere>$' debian/patches/0001-some-patch.patch ; then
	echo "patch has wrong/missing From line:" >&2
	cat debian/patches/0001-some-patch.patch >&2
fi
cat > ../results.expected <<'EOF'
 morenumbers |   18 	18 +	0 -	0 !
 numbers     |    1 	0 +	1 -	0 !
 2 files changed, 18 insertions(+), 1 deletion(-)
EOF
diffstat -f 0 debian/patches/0001-some-patch.patch > ../results
compare_results "patch contents after init"

check_gitlog "init" "noauthor" <<EOF
Import some.orig.tar.gz, tags: upstream1
Initialize git-dpm, parents: add debian, some patch
add debian, parents: Import some.orig.tar.gz
some patch, parents: Import some.orig.tar.gz, tags: patches
EOF

git rm lines
echo "/something" > .gitignore
git add .gitignore
git commit -m "rlagi"

$gitdpm checkout-patched

check_gitlog "init + checkout-patched" "noauthor" <<EOF
Import some.orig.tar.gz, tags: upstream1
some patch, parents: Import some.orig.tar.gz, tags: patches
EOF

echo "new file"  > new
git add new
git commit -m "add new"

EDITOR=true $gitdpm merge-patched
$gitdpm update-patches -m "update"

check_gitlog "init + some patches" "noauthor" <<EOF
Import some.orig.tar.gz, tags: upstream1
Initialize git-dpm, parents: add debian, some patch
add debian, parents: Import some.orig.tar.gz
add new, parents: some patch
merge patched-some into some, parents: rlagi, add new
rlagi, parents: Initialize git-dpm
some patch, parents: Import some.orig.tar.gz, tags: patches
update, parents: merge patched-some into some
EOF

git ls-tree -r HEAD | sed -e 's# [0-9a-f]*\(	debian/\(.git-dpm\|patches\)\)# X\1#' >../results
cat > ../results.expected <<EOF
100644 blob 0c9cf93eba7598cd5f1c15c82cd8638ede63f621	.gitignore
100644 blob X	debian/.git-dpm
100644 blob a7f8d9e5dcf3a68fdd2bfb727cde12029875260b	debian/control
100644 blob X	debian/patches/0001-some-patch.patch
100644 blob X	debian/patches/0002-add-new.patch
100644 blob X	debian/patches/series
100644 blob 163aaf8d82b6c54f23c45f32895dbdfdcc27b047	debian/source/format
100644 blob 56d8e57bf9f9b45c15d8aacef6f00d1f48c75139	morenumbers
100644 blob fa49b077972391ad58037050f2a75f74e3671e92	new
100644 blob 051c29820ed106f8865c540dc6483142b5e490c8	numbers
EOF
compare_results "tree after init + some patch (with deletions and .gitignore in debian)"

cp debian/patches/series ../results
cat > ../results.expected <<EOF
0001-some-patch.patch
0002-add-new.patch
EOF
compare_results "series after two patches added"

git branch -d upstream-some
EDITOR="true" $gitdpm import-tar "$EXDIR/some2.orig.tar.gz"
git ls-tree -r HEAD  >../results
cat > ../results.expected <<EOF
100644 blob 822216280fd1de308f55f27cdd7a4196a8f5a189	debian/othercruft
100644 blob 6ebc1a6235e468ee35924e6346280d4c9f16074d	lines
100644 blob 08fe19ca4d2f79624f35333157d610811efc1aed	numbers
EOF
compare_results "import-tar some2.orig.tar.gz"
git checkout -b upstream-some

$gitdpm new-upstream --rebase-patched "$EXDIR/some2.orig.tar.gz"

check_gitlog "new-upstream with rebase-patched" "noauthor" <<EOF
Import some2.orig.tar.gz
add new, parents: some patch
some patch, parents: Import some2.orig.tar.gz
EOF

EDITOR="sed -e 's/^pick/edit/' -i" git rebase -i upstream-some
git commit -m "some patch2" --amend
git rebase --continue
git commit -m "add new2" --amend
echo here
git rebase --continue
echo there
git rev-parse HEAD

check_gitlog "new-upstream with rebase-patched and manual patch editing" "noauthor" <<EOF
Import some2.orig.tar.gz
add new2, parents: some patch2
some patch2, parents: Import some2.orig.tar.gz
EOF
$gitdpm update-patches --amend -m "new upstream"

git ls-tree -r HEAD | sed -e 's# [0-9a-f]*\(	debian/\(.git-dpm\|patches\)\)# X\1#' >../results
cat > ../results.expected <<EOF
100644 blob 0c9cf93eba7598cd5f1c15c82cd8638ede63f621	.gitignore
100644 blob X	debian/.git-dpm
100644 blob a7f8d9e5dcf3a68fdd2bfb727cde12029875260b	debian/control
100644 blob X	debian/patches/0001-some-patch2.patch
100644 blob X	debian/patches/0002-add-new2.patch
100644 blob X	debian/patches/series
100644 blob 163aaf8d82b6c54f23c45f32895dbdfdcc27b047	debian/source/format
100644 blob 56d8e57bf9f9b45c15d8aacef6f00d1f48c75139	morenumbers
100644 blob fa49b077972391ad58037050f2a75f74e3671e92	new
100644 blob 684fc2fb655382ffe98c312cd3ff0ccaca588fd0	numbers
EOF
compare_results "tree after new upstream"

check_gitlog "init + some patches" "noauthor" <<EOF
Import some.orig.tar.gz, tags: upstream1
Import some2.orig.tar.gz
Initialize git-dpm, parents: add debian, some patch
add debian, parents: Import some.orig.tar.gz
add new, parents: some patch
add new2, parents: some patch2
merge patched-some into some, parents: rlagi, add new
new upstream, parents: update, add new2
rlagi, parents: Initialize git-dpm
some patch, parents: Import some.orig.tar.gz, tags: patches
some patch2, parents: Import some2.orig.tar.gz
update, parents: merge patched-some into some
EOF

git checkout -f some-upstream
git reset --hard upstream1
git branch -D some
$gitdpm init "$EXDIR/some.orig.tar.gz" some-upstream some-upstream patches

git ls-tree -r HEAD | sed -e 's# [0-9a-f]*\(	debian/\(.git-dpm\|patches\)\)# X\1#' >../results
cat > ../results.expected <<EOF
100644 blob X	debian/.git-dpm
100644 blob 822216280fd1de308f55f27cdd7a4196a8f5a189	debian/cruft
100644 blob X	debian/patches/0001-some-patch.patch
100644 blob X	debian/patches/series
100644 blob 163aaf8d82b6c54f23c45f32895dbdfdcc27b047	debian/source/format
100644 blob 06baef206a8179d004448aca0d8e61b312bc397a	lines
100644 blob 56d8e57bf9f9b45c15d8aacef6f00d1f48c75139	morenumbers
100644 blob 051c29820ed106f8865c540dc6483142b5e490c8	numbers
EOF
compare_results "to init (without debian branch)"
fi

if testcaseenabled update-patches ; then

$gitdpm empty-tree "Start"
git checkout -b update-patches

true > ../results.expected
git ls-files > ../results
compare_results "empty-tree creates an empty tree"

true > ../results.expected
git ls-files -o > ../results
compare_results "empty-tree leaves no spurious files"

echo a > a
echo b > b
echo c > c
git add a b c
git commit --amend -m "Start"
h="$(git rev-parse HEAD)"

$gitdpm empty-tree "new"
echo x > x
git add x
git commit --amend -m "new"
n="$(git rev-parse HEAD)"

git branch patched-update-patches

git checkout update-patches

mkdir -p debian
cat > debian/.git-dpm <<EOF
# Dummy git-dpm control file
$h
$h
$h
$n
none.orig.tar
0000000000000000000000000000000000000000
0
EOF

git add debian/.git-dpm
git commit -m "old"

$gitdpm merge-patched

echo debian/.git-dpm > ../results.expected
echo x >> ../results.expected
git ls-files > ../results
compare_results "merge-patched has the correct files"

check_gitlog "merge-patched" "noauthor" <<EOF
Start
merge patched-update-patches into update-patches, parents: old, new
new
old, parents: Start
EOF

true > ../results.expected
git ls-files -o > ../results
compare_results "merge-patched leaves no files behind"


fi # update-patches

if testcaseenabled components ; then

EDITOR=true $gitdpm import-tar -- "$EXDIR"/aaaa_0.orig-bla.tar.gz
git tag a0bla
pristine-tar commit "$EXDIR"/aaaa_0.orig-bla.tar.gz a0bla

$gitdpm import-dsc -b components -- "$EXDIR"/aaaa_0-1.dsc

$gitdpm update-patches

git checkout components

cp "$EXDIR"/aaaa_0.orig.tar.gz ../
$gitdpm prepare

pristine-tar commit "$EXDIR"/aaaa_0.orig.tar.gz upstream-components

git checkout upstream-components

git read-tree --prefix="bla/" "a0bla:"
git commit -m "source with component bla/ added"
git checkout HEAD bla

$gitdpm new-upstream --component "$EXDIR"/aaaa_0.orig-bla.tar.gz ../aaaa_0.orig.tar.gz
git checkout components

cat debian/.git-dpm

rm ../aaaa_0.orig.tar.gz
$gitdpm prepare

if ! test -f ../aaaa_0.orig.tar.gz ; then
	echo "prepare did not create ../aaaa_0.orig.tar.gz" >&2
	exit 1
fi
if ! test -f ../aaaa_0.orig-bla.tar.gz ; then
	echo "prepare did not create ../aaaa_0.orig-bla.tar.gz" >&2
	exit 1
fi

$gitdpm status
rm ../aaaa_0.orig-bla.tar.gz

ec=0
$gitdpm status || ec=$?
if [ "$ec" -eq 0 ] ; then
	echo "git-dpm status did not return an error, though it should have (aaaa_0.orig-bla.tar.gz missing)" >&2
	exit 1
fi

cp "$EXDIR"/aaaa_0.orig-bla.tar.gz ../

$gitdpm status

$gitdpm dch -- -v "0extra-1" "test wrong upstream version"

ec=0
$gitdpm status || ec=$?
if [ "$ec" -eq 0 ] ; then
	echo "git-dpm status did not return an error, though it should have (version in debian/changelog does not match version in orig filenames)" >&2
	exit 1
fi

fi # components

if testcaseenabled componentsimport1 ; then

git branch -D pristine-tar || true

$gitdpm import-dsc --ptc -b ci1 "$EXDIR"/bbbb_0-1.dsc
$gitdpm tag --named

git ls-tree -r --name-only bbbb-upstream-0 > ../results
cat > ../results.expected <<'EOF'
bla/bla-a
bla/bla-b
upstream1
upstream2
EOF
compare_results "upstream tree after dsc-import bbbb_0-1.dsc"

$gitdpm import-dsc --ptc -b ci1 "$EXDIR"/bbbb_0-2.dsc
$gitdpm tag --named --refresh-upstream

git ls-tree -r --name-only bbbb-upstream-0 > ../results
cat > ../results.expected <<'EOF'
baz/baz-a
baz/baz-b
bla/bla-a
bla/bla-b
upstream1
upstream2
EOF
compare_results "upstream tree after dsc-import bbbb_0-2.dsc"

$gitdpm import-dsc --ptc -b ci1 "$EXDIR"/bbbb_0-3.dsc
$gitdpm tag --named --refresh-upstream

git ls-tree -r --name-only bbbb-upstream-0 > ../results
cat > ../results.expected <<'EOF'
baz/baz-a
baz/baz-b
upstream1
upstream2
EOF
compare_results "upstream tree after dsc-import bbbb_0-3.dsc"

deterministic_log noauthor > ../results
LC_ALL=C sort > ../results.expected << 'EOF'
Import bbbb_0.orig.tar.bz2
Import bbbb_0.orig.tar.bz2, bbbb_0.orig-bla.tar.gz, parents: Import bbbb_0.orig.tar.bz2, tags: bbbb-patched-0-1
Import bbbb 0-1, parents: Import bbbb_0-1.dsc, tags: bbbb-debian-0-1
Import bbbb_0-1.dsc, parents: Import bbbb_0.orig.tar.bz2, bbbb_0.orig-bla.tar.gz
Add  baz, parents: Import bbbb_0.orig.tar.bz2, bbbb_0.orig-bla.tar.gz, tags: bbbb-patched-0-2
Import bbbb 0-2, parents: Import bbbb 0-1, Import bbbb_0-2.dsc, tags: bbbb-debian-0-2
Import bbbb_0-2.dsc, parents: Add  baz
Import bbbb_0.orig.tar.bz2, bbbb_0.orig-baz.tar.bz2, parents: Add  baz, tags: bbbb-patched-0-3, bbbb-upstream-0
Import bbbb 0-3, parents: Import bbbb 0-2, Import bbbb_0-3.dsc, tags: bbbb-debian-0-3
Import bbbb_0-3.dsc, parents: Import bbbb_0.orig.tar.bz2, bbbb_0.orig-baz.tar.bz2
EOF
compare_results "import-dsc with components"

cp -- "$EXDIR"/bbbb_0.orig-baz.tar.bz2 ../bbbb_1.orig-baz.tar.bz2

$gitdpm import-new-upstream --ptc --component ../bbbb_1.orig-baz.tar.bz2 -- "$EXDIR"/bbbb_1.orig.tar.gz
rm ../bbbb_1.orig-baz.tar.bz2
$gitdpm dch --amend -- -v 1-1 "new upstream version"
# avoid problems with different git versions splitting the subject differently
git commit --amend -m "new"
$gitdpm tag --named


deterministic_log noauthor > ../results
LC_ALL=C sort > ../results.expected << 'EOF'
Import bbbb_0.orig.tar.bz2
Import bbbb_0.orig.tar.bz2, bbbb_0.orig-bla.tar.gz, parents: Import bbbb_0.orig.tar.bz2, tags: bbbb-patched-0-1
Import bbbb 0-1, parents: Import bbbb_0-1.dsc, tags: bbbb-debian-0-1
Import bbbb_0-1.dsc, parents: Import bbbb_0.orig.tar.bz2, bbbb_0.orig-bla.tar.gz
Add  baz, parents: Import bbbb_0.orig.tar.bz2, bbbb_0.orig-bla.tar.gz, tags: bbbb-patched-0-2
Import bbbb 0-2, parents: Import bbbb 0-1, Import bbbb_0-2.dsc, tags: bbbb-debian-0-2
Import bbbb_0-2.dsc, parents: Add  baz
Import bbbb_0.orig.tar.bz2, bbbb_0.orig-baz.tar.bz2, parents: Add  baz, tags: bbbb-patched-0-3, bbbb-upstream-0
Import bbbb 0-3, parents: Import bbbb 0-2, Import bbbb_0-3.dsc, tags: bbbb-debian-0-3
Import bbbb_0-3.dsc, parents: Import bbbb_0.orig.tar.bz2, bbbb_0.orig-baz.tar.bz2
Import bbbb_1.orig.tar.gz
Import bbbb_1.orig.tar.gz, bbbb_1.orig-baz.tar.bz2, parents: Import bbbb_0.orig.tar.bz2, bbbb_0.orig-baz.tar.bz2, Import bbbb_1.orig.tar.gz, tags: bbbb-patched-1-1, bbbb-upstream-1
new, parents: Import bbbb 0-3, Import bbbb_1.orig.tar.gz, bbbb_1.orig-baz.tar.bz2, tags: bbbb-debian-1-1
EOF
compare_results "import-new-upstream with components"

git ls-tree -r --name-only bbbb-upstream-1 > ../results
cat > ../results.expected <<'EOF'
baz/baz-a
baz/baz-b
upstream-a
upstream-b
EOF
compare_results "upstream tree after import-new-upstream with components"

cat > ../results.expected <<EOF
bbbb_0.orig-baz.tar.bz2.id $(git rev-parse 'bbbb-upstream-0:baz')
bbbb_0.orig-bla.tar.gz.id $(git rev-parse 'bbbb-patched-0-2:bla')
bbbb_0.orig.tar.bz2.id $(git rev-parse 'bbbb-patched-0-1^1:')
bbbb_1.orig-baz.tar.bz2.id $(git rev-parse 'bbbb-upstream-1:baz')
bbbb_1.orig.tar.gz.id $(git rev-parse 'bbbb-upstream-1^2:')
EOF
git ls-tree --name-only pristine-tar | grep '.*\.id' | while read name ; do
	echo "$name $(git cat-file blob pristine-tar:"$name")"
done > ../results
compare_results "pristine-tar by --ptc commited the right files and trees"

fi #componentsimport1

if testcaseenabled specialbranchnames ; then

git config branch.deb/name/master.dpmPatchedBranch deb/name/patched
git config branch.deb/name/master.dpmUpstreamBranch deb/name/upstream
EDITOR=true $gitdpm import-new-upstream --init --branch deb/name/master "$EXDIR"/aaaa_0.orig.tar.gz
if test x"$(git symbolic-ref HEAD)" != x"refs/heads/deb/name/master" ; then
	echo "Incorrect branch name after import-new-upstream --init --branch" >&2
	exit 1
fi
$gitdpm testbranchnames > ../results
cat > ../results.expected <<EOF
'DEBIANBRANCH' is 'deb/name/master' ($(git rev-parse deb/name/master))
'UPSTREAMBRANCH' is 'deb/name/upstream' ($(git rev-parse deb/name/upstream))
'PATCHEDBRANCH' is 'deb/name/patched' (no such head)
EOF
compare_results "branch names after import-new-upstream with strange names"
git checkout deb/name/upstream
cat > ../results.expected <<EOF
'DEBIANBRANCH' is 'deb/name/master' ($(git rev-parse deb/name/master))
'UPSTREAMBRANCH' is 'deb/name/upstream' ($(git rev-parse deb/name/upstream))
'PATCHEDBRANCH' is 'deb/name/patched' (no such head)
EOF
$gitdpm checkout-patched
cat > ../results.expected <<EOF
'DEBIANBRANCH' is 'deb/name/master' ($(git rev-parse deb/name/master))
'UPSTREAMBRANCH' is 'deb/name/upstream' ($(git rev-parse deb/name/upstream))
'PATCHEDBRANCH' is 'deb/name/patched' ($(git rev-parse deb/name/patched))
EOF

fi #specialbranchnames

if $sometestsfound ; then
	echo "################################# END ###############################"
	echo "$testtodo tests successfull..."
else
	echo "Unknown test $testtodo"
	exit 1
fi
exitmessage=""
exitcode=0
exit 0
